#Get the weekly calender

#PROGRAMM_NAME=/var/www/html/play/weekly-programm.html
PROGRAMM_NAME=weekly-programm.html

wget -q -O $PROGRAMM_NAME "https://airtime.bavariabeats.de/embed/weekly-program"
sed -i 's;http://;https://;g' $PROGRAMM_NAME
sed -i 's;<a href="https://libretime.org" target="_blank">Powered by LibreTime</a>;<a href="https://bavariabeats.de" target="_blank">#elektronischbayrischgut</a>;g' $PROGRAMM_NAME
sed -i "s;<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>;;g" $PROGRAMM_NAME


# Remove unused css statement

sed -i 's;<link type="text/css" rel="Stylesheet" href="/assets/css/schedule.css" />;;g' $PROGRAMM_NAME
sed -i 's;<!-- IFRAME-CSS-ADDITIONS -->;;g' $PROGRAMM_NAME

sed -i 's\<head>\<head><style>*{color: #d1d1d1} .weekly-schedule-widget-footer {display:none} .schedule_content {background:transparent!important}</style>\g' $PROGRAMM_NAME

